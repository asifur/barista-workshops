import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AgegateComponent } from './components/agegate/agegate.component';
import { HomeComponent } from './components/home/home.component';

import { RouterModule, Routes } from '@angular/router';
//import { FormsModule } from '@angular/forms';

import { AgmCoreModule } from '@agm/core';
import { StoreLocatorComponent } from './components/store-locator/store-locator.component';
import { CookieConsentComponent } from './components/cookie-consent/cookie-consent.component';

import { ScrollToModule } from 'ng2-scroll-to-el';
import { AnimateOnScrollModule } from 'ng2-animate-on-scroll';
import { HeaderComponent } from './components/header/header.component';

import { Helper } from "./helpers/app.helper";

import { HttpClientModule } from '@angular/common/http';


// ALL ROUTES
// Define all routes
const appRoutes: Routes = [
  // { path: 'agegate', component: AgegateComponent },
  { path: '', component: HomeComponent },
  //{ path: 'home', component: HomeComponent },
  //{ path: 'stores', component: StoreLocatorComponent },
  //{ path: '', redirectTo: 'agegate', pathMatch: 'full' },
];


@NgModule({
  declarations: [
    AppComponent,
    AgegateComponent,
    HomeComponent,
    StoreLocatorComponent,
    CookieConsentComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    //FormsModule,
    RouterModule.forRoot(
      appRoutes,{
        //useHash: true
      }
    ),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDJ0ZVAlFCvujqZ7v2PB2y8Hc0YleY6UjU'
    }),
    ScrollToModule.forRoot(),
    AnimateOnScrollModule.forRoot()    
  ],
  providers: [
    Title, Helper
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
