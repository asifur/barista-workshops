import { Component, OnInit, Renderer2 } from '@angular/core';
import { Title }  from '@angular/platform-browser';
import { Router } from '@angular/router';
import AgeGate from 'agegate';
import {Helper} from "../../helpers/app.helper";

@Component({
  selector: 'app-agegate',
  templateUrl: './agegate.component.html',
  styleUrls: ['./agegate.component.css']
})
export class AgegateComponent implements OnInit {

	// Variable declaration
	public hasError = false;
	public isValid = true;
	public cookie = false;

	// Error states
	public errorMsg = {
		refuse: "Minimale leeftijd : 12 jaar.",
		invalid: "U moet een geldige datum invoeren",
	};

	// -------------------------------------------------------------------
	// Class constructor
	// -------------------------------------------------------------------
	constructor(private router: Router, private titleService: Title, public helper: Helper, public renderer: Renderer2) {

		let isOldEnough = this.helper.getCookie("old_enough");
		if(isOldEnough && isOldEnough != ""){
			this.cookie = true;
		}else{
			this.renderer.addClass(document.body, 'modal-open');
			this.renderer.addClass(document.querySelector('html'), 'modal-open');
		}

	}

	// -------------------------------------------------------------------
	// Initializes the class
	// -------------------------------------------------------------------
	ngOnInit() {

		// Setup the agegate functionality
		let options = {
			form: document.getElementById('agegate'),
			age: 12,
			expiry: Infinity		
		};
		let gate = new AgeGate(options, (err) => {
			if (err) {
				//console.log(err);
				if(gate.formData.day > 31 || gate.formData.month > 12){
					this.isValid = false;
					this.hasError = false;
				}else{
					this.hasError = true;
					this.isValid = true;
				}
			}else {
				//console.log('Welcome to twix');
				this.cookie = true;
				this.renderer.removeClass(document.body, 'modal-open');
				this.renderer.removeClass(document.querySelector('html'), 'modal-open');
			}
		});


	}

	// -------------------------------------------------------------------
	// Validates the Age before submit
	// -------------------------------------------------------------------

	limitChars(element,max){
	    var max_chars = max;
	    if(element.value.length > max_chars) {
	        element.value = element.value.substr(0, max_chars);
	    }
	}
	addZero(element){
		if(element.value.length != 1)return;
		element.value = (element.value < 10 ? '0' : '') + element.value;
		if(element.value == '00')element.value = '01';
		
	}
	dayComplete(element,next){
		if(element.value.length >= 2){
			this.addZero(element);
			document.getElementById(next).focus();
		}
	}

	validateYear(element){
		if(element.value.length < 4)element.value = "";
	}

}
