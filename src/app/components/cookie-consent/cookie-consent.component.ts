import { Component, OnInit } from '@angular/core';
import {Helper} from "../../helpers/app.helper";

@Component({
  selector: 'app-cookie-consent',
  templateUrl: './cookie-consent.component.html',
  styleUrls: ['./cookie-consent.component.css']
})
export class CookieConsentComponent implements OnInit {

	public acted = false;

    // -------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------
    constructor(public helper: Helper) { 
        /*		if(this.helper.getCookie("cookie-accpeted") && this.helper.getCookie("cookie-accpeted") != ""){
        this.acted = true;
        }*/
    }

    ngOnInit() {
    }

    // -------------------------------------------------------------------
    // Hides the cookie consetnt message
    // -------------------------------------------------------------------
  	hideCookieMsg($event, response){
  		$event.preventDefault();
/*  		console.log(response);
  		if(response == true){
  			this.helper.setCookie("cookie-accpeted",true,10);
  		}else{
  			this.helper.setCookie("cookie-accpeted",false,10);
  		}*/
  		this.acted = true;
  	}

}