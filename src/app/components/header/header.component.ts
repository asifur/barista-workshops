import { Component, OnInit, Renderer2 } from '@angular/core';
import { ScrollToService } from 'ng2-scroll-to-el';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

	public openNav = false;
	constructor(public renderer: Renderer2, private scrollService: ScrollToService) { }

	isActive = true;
	isMobile = false;

	ngOnInit() {
		if(window.innerWidth <= 1024)this.isMobile = true;
		//this.isMobile = true;
	}

    // -------------------------------------------------------------------
    // Open & Close the navigation
    // -------------------------------------------------------------------
	openNavigation(){

		this.openNav = true;
		this.renderer.addClass(document.querySelector('header .nav-btn-group'), 'anim');
		this.renderer.addClass(document.querySelector('header .nav-btn-close'), 'anim');
		// Disable all scrolling
/*		if(this.isMobile){
			this.renderer.addClass(document.body, 'menu-open');
			this.renderer.addClass(document.querySelector('html'), 'menu-open');			
		}*/

	}

	closeNavigation(){

		this.openNav = false;
		this.renderer.removeClass(document.querySelector('header .nav-btn-group'), 'anim');
		this.renderer.removeClass(document.querySelector('header .nav-btn-close'), 'anim');
		// Enable all scrolling
/*		if(this.isMobile){
			this.renderer.removeClass(document.body, 'menu-open');
			this.renderer.removeClass(document.querySelector('html'), 'menu-open');			
		}*/

	}

	activateItem($event){

		// Enable all scrolling
/*		if(this.isMobile){
			this.renderer.removeClass(document.body, 'menu-open');
			this.renderer.removeClass(document.querySelector('html'), 'menu-open');			
		}	*/	

		var menuArr = document.querySelectorAll('.nav-menu li');
		for (var i = 0; i < menuArr.length; i++) {
		    menuArr[i].classList.remove('active');
		}
		$event.currentTarget.parentElement.classList.add('active');
		this.openNav = false;
		this.renderer.removeClass(document.querySelector('header .nav-btn-group'), 'anim');
		this.renderer.removeClass(document.querySelector('header .nav-btn-close'), 'anim');
	}

	scrollToElm(id,duration,offset) {
		this.scrollService.scrollTo(document.getElementById(id),duration,offset);
    }


}
