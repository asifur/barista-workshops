import { Component, OnInit } from '@angular/core';
import { Title }  from '@angular/platform-browser';
import { Router, NavigationEnd } from '@angular/router';
import { Helper } from "../../helpers/app.helper";
import { ScrollToService } from 'ng2-scroll-to-el';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	public openNav = false;

	constructor(private router: Router, private titleService: Title, public helper: Helper, private scrollService: ScrollToService) {
		this.titleService.setTitle("IS TWIX & KOFFIE DÉ WINNENDE COMBI?");
	}

    // -------------------------------------------------------------------
    // Init home page
    // -------------------------------------------------------------------
	ngOnInit() {

	}

	scrollToElm(id,duration,offset) {
        this.scrollService.scrollTo(document.getElementById(id),duration,offset);
    }

}
