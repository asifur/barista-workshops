import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-store-locator',
  templateUrl: './store-locator.component.html',
  styleUrls: ['./store-locator.component.css']
})
export class StoreLocatorComponent implements OnInit {

	// Initial Data for Centering the MAP
	// ---------------------------------------------------
	centerLat: number = 53.20751675;
	centerLong: number = 6.15076370517544;
	zoom: number = 6;
	mapType: string = 'roadmap';

	// Store arrays
	allStores = new Array();
	stores = new Array();

	// Error States 
	errors = {
		pattern: true,
		msg: false
	}

	// Model binding variable
	public zipcode;

	// Constructor
	// ---------------------------------------------------
	constructor(private httpService: HttpClient) { 
		this.zipcode = "";
	}

	// Init class
	// ---------------------------------------------------
	ngOnInit() {

		this.httpService.get('./assets/data/stores.json').subscribe(
			data => {
				this.allStores = data as string [];
				this.stores = this.allStores;
			},
			(err: HttpErrorResponse) => {
				console.log (err.message);
			}
		);

	}

	// Form submit handler
	// ---------------------------------------------------
	onSubmit(zipcode){

		// Check for null values
		if(this.zipcode.length == 0)return;
		this.zipcode = zipcode.value;

		// Check validation & update map
		if(!this.errors.pattern){
			this.updateMap();
			this.errors.msg = false;
		}else{
			this.errors.msg = true;
		}

	}

	// Check zip codes for validation with keypress event
	// ---------------------------------------------------
	validateZip(zipcode){
		this.zipcode = zipcode.value;
		this.isValidDutchZipcode(this.zipcode);
	}

	// Validates the zip code for dutch
	// ---------------------------------------------------
	isValidDutchZipcode(zipcode){

		var pattern1 = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i; // 1234 AA
		var pattern2 = /^[a-z]{2} ?(?!sa|sd|ss)[1-9][0-9]{3}$/i; // AA 1234
		if(pattern1.test(zipcode) || pattern2.test(zipcode)){
			//console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaa');
			this.errors.pattern = false;
			return;
		}
		this.errors.pattern = true;
	}

	// Updates the google map based on the input zipcode
	// ---------------------------------------------------
	updateMap(){

		//this.stores = [];
		var total = this.allStores.length;
		for(var i=0;i<total;i++){
			if(this.getFlatZipcode(this.allStores[i].zipcode) == this.getFlatZipcode(this.zipcode) ){
				this.centerLat = this.allStores[i].lat;
				this.centerLong = this.allStores[i].long;
				this.zoom = 10;
			}
			this.stores.push(this.allStores[i]);
		}

	}

	// Zip code formatting for matching
	// ---------------------------------------------------
	getFlatZipcode(zipcode){
		//return zipcode.replace(/\s/g,'').toLowerCase(); // all lowercase and trim spaces
		return zipcode.replace(/\s/g,'').replace(/\D/g,''); // trim and numbers only
	}

	// Limit the number of chracters
	// -----------------------------------------------------------
	limitChars(element,max){
	    var max_chars = max;
	    if(element.value.length > max_chars) {
	        element.value = element.value.substr(0, max_chars);
	    }
	}


}