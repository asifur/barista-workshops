    // Scroll to top in page load
    window.onbeforeunload = function () {
        window.scrollTo(0,0);
    }

    // Get the viewport height and use it for the intro block
    function setWindowHeight(){
        var windowHeight = window.innerHeight;
        var windowWidth = window.innerWidth;
        if(windowWidth < 768 && windowHeight > 670 && windowHeight < 736){
            document.getElementById('intro-block').style.height = windowHeight + "px";
            //console.log('height adjusted !!');
        }

    }

    // Show the page after load
    window.onload = function(){
        // setWindowHeight();
        //document.querySelector('body').classList.remove('page-loading');
        // Cross browser (IE < 10)
        var body = document.querySelector('body');
        body.className = body.className.replace(/\bpage-loading\b/g, "");
        new WOW().init();
    }
    window.onresize = function(event) {
        // setWindowHeight();
    };


    window.addEventListener('orientationchange', function(){
        window.setTimeout(function() {
            // setWindowHeight();
        },200);
    });


    // -----------------------------------------------------------------------------------------------------------------    
    // ACTIVATE MENU WITH SCROLL - START
    // -----------------------------------------------------------------------------------------------------------------    

    var scrollOffset = 150;
    
    if(window.innerWidth < 768){
        //scrollOffset = 600;
    }

    // SCROLL CHECK
    function getBodyScrollTop () { 
        //const el = document.scrollingElement || document.documentElement;
        var scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
        return scrollTop;
    }

    function isScrolledToBottom(){
        if ((window.innerHeight + getBodyScrollTop()) >= document.body.offsetHeight) {
            return true;
        }
        return false;
    }

    // Activate Menu Item
    function activateMenuWithScroll(){

        var sections = ['intro-block','combination','store-locator','footer'];

        var allMenuItem = document.querySelectorAll('.nav-menu li');
        var bodyScrollTop = getBodyScrollTop();

        if(isScrolledToBottom()){
            for(var j=0; j < allMenuItem.length; j++){
                allMenuItem[j].classList.remove('active');
            }
            document.querySelector('.nav-menu li[data-section="footer"]').classList.add('active');  
            return;          
        }
        
        for(var i=0;i<sections.length;i++){
            var section = document.getElementById(sections[i]);
            var elTop = section.getBoundingClientRect().top;
            if(elTop + scrollOffset <= bodyScrollTop && elTop <= scrollOffset){
                //console.log('body:' + bodyScrollTop,'el-name:' + sections[i], 'el-top:' + section.getBoundingClientRect().top, 'el-bottom:'+section.getBoundingClientRect().bottom);
                for(var j=0; j < allMenuItem.length; j++){
                    allMenuItem[j].classList.remove('active');
                }
                document.querySelector('.nav-menu li[data-section="' + sections[i] + '"]').classList.add('active');
            }
        }
    }

    // Enable Sticky Menu on Scroll
    function enableStickyMenu(){
        var scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
        if(scrollTop > 50){
            document.querySelector('header .sticky-header').classList.add('in');
        }else{
            document.querySelector('header .sticky-header').classList.remove('in');
        }
    }


    // Winodw scroll event
    window.onscroll = function(){
        activateMenuWithScroll();
        enableStickyMenu();
    }

    // -----------------------------------------------------------------------------------------------------------------    
    // ACTIVATE MENU WITH SCROLL - END
    // -----------------------------------------------------------------------------------------------------------------   